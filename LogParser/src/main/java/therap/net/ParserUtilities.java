package therap.net;

import therap.net.LogView;
import therap.net.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author arafat
 * @since 11/7/16
 */
public class ParserUtilities {

    private LogView logView;

    private String systemTime;
    private String uri;
    private String type;
    private String responseTime;


    public ParserUtilities() {

    }

    public String parseSystemTime(String line) {

        Pattern timePattern = Pattern.compile("[0-9]+:[0-9]+:[0-9]+,[0-9]+");
        Matcher timeMatcher = timePattern.matcher(line);
        timeMatcher.find();
        String systemTime = timeMatcher.group().substring(0, 2);

        return systemTime;


    }

    public String parseUri(String line) {
        Pattern uriPattern = Pattern.compile("URI=\\[(.*?)\\]");
        Matcher uriMatcher = uriPattern.matcher(line);

        if (uriMatcher.find()) {
            return uriMatcher.group(1);
        } else {
            return "false";
        }

    }

    public String parseType(String line) {

        Pattern getOrPostPattern = Pattern.compile("[G|P],+");
        Matcher getOrPostMatcher = getOrPostPattern.matcher(line);
        String res = "";

        if (getOrPostMatcher.find()) {
            if (getOrPostMatcher.group().contains("G")) {
                res = "G";

            } else if (getOrPostMatcher.group().contains("P")) {
                res = "P";
            }
        } else {
            res = "null";
        }
        return res;

    }

    public String parseResponseTime(String line) {
        Pattern patternTime = Pattern.compile("time=[0-9]+ms");
        Matcher m = patternTime.matcher(line);

        boolean b1 = m.find();


        if (b1) {

            String time = m.group();


            Pattern subpatternResponseTime = Pattern.compile("\\d+");

            Matcher mResponseTime = subpatternResponseTime.matcher(time);

            boolean b2 = mResponseTime.find();

            return mResponseTime.group();

        } else {
            return "0";

        }

    }


    public HashMap<String, Log> retrieveListOfAllEntries(String logFile, boolean sort) {
        HashMap<String, Log> logList = new HashMap<String, Log>();
        int totalResponseTime = 0;

        try {

            FileInputStream fstream = new FileInputStream(logFile);

            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;

            ParserUtilities parserUtilities = new ParserUtilities();


            int getCount = 0;
            int postCount = 0;
            HashSet hashSet = new HashSet();
            int hashSetSize = 0;

            while ((strLine = br.readLine()) != null) {

                Log log = new Log();

                systemTime = parserUtilities.parseSystemTime(strLine);

                uri = parserUtilities.parseUri(strLine);

                type = parserUtilities.parseType(strLine);

                responseTime = parserUtilities.parseResponseTime(strLine);

                if (logList.containsKey(systemTime)) {


                    if (!uri.equals("false")) {
                        hashSet.add(uri);
                        hashSetSize = hashSet.size();
                        log.setUriCount(hashSetSize);
                    }

                    if (type.equals("G")) {
                        getCount++;
                        log.setTotalGCount(getCount);
                        log.setTotalPCount(postCount);

                    } else if (type.equals("P")) {
                        postCount++;
                        log.setTotalPCount(postCount);
                        log.setTotalGCount(getCount);
                    }

                    totalResponseTime += Integer.valueOf(responseTime);

                    log.setTotalResponseTime(totalResponseTime);

                } else {
                    getCount = 0;
                    postCount = 0;
                    totalResponseTime = 0;

                    if (hashSet.size() > 0) {
                        hashSet.clear();
                    }


                    if (!uri.equals("false")) {
                        hashSet.add(uri);
                        hashSetSize = hashSet.size();
                        log.setUriCount(hashSetSize);
                    }

                    if (type.equals("G")) {
                        getCount++;
                        log.setTotalGCount(getCount);
                        log.setTotalPCount(postCount);

                    } else if (type.equals("P")) {
                        postCount++;
                        log.setTotalPCount(postCount);
                        log.setTotalGCount(getCount);
                    }

                    totalResponseTime += Integer.valueOf(responseTime);

                    log.setTotalResponseTime(totalResponseTime);


                }


                logList.put(systemTime, log);

            }
            fstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sort) {
            logList = sortHashMap(logList);
        }


        return logList;
    }


    public HashMap<String, Log> sortHashMap(HashMap<String, Log> unsortedHashMap) {
        List<Map.Entry<String, Log>> entryList = new LinkedList<Map.Entry<String, Log>>(unsortedHashMap.entrySet());


        Collections.sort(entryList, new Comparator<Map.Entry<String, Log>>() {
            @Override
            public int compare(Map.Entry<String, Log> o1, Map.Entry<String, Log> o2) {
                return (o1.getValue()
                        .compareTo(o2.getValue()));
            }
        });


        HashMap<String, Log> sortedLogs = new LinkedHashMap<>();

        for (Map.Entry<String, Log> entry : entryList) {
            sortedLogs.put(entry.getKey(), entry.getValue());
        }
        return sortedLogs;
    }

    public void showSummary(HashMap<String, Log> logList) {

        logView = new LogView();
        logView.printLogSummary(logList);

    }


}
