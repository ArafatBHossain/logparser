package therap.net;


import java.util.HashMap;

/**
 * @author arafat
 * @since 11/7/16
 */
public class LogView {

    public void printLogSummary(HashMap<String, Log> logList) {

        final int HOURS = 12, ZERO = 0;

        System.out.printf("%15s %27s %28s %25s\n", "TIME", "G/P", "Unique URI Count", "Total Response Time");

        int startTime, endTime;
        String startKey = "am", endKey = "pm";

        for (String s : logList.keySet()) {

            startTime = Integer.valueOf(s);
            endTime = Integer.valueOf(s) + 1;


            if (startTime >= HOURS) {
                startKey = "pm";
                if (startTime >= (HOURS + 1)) {
                    startTime -= HOURS;
                }

            } else {
                if (startTime == ZERO) {
                    startTime = HOURS;
                }
            }


            if (endTime >= HOURS) {
                endKey = "pm";
                if (endTime >= (HOURS + 1)) {
                    endTime -= HOURS;
                }

            } else {
                if (endTime == ZERO) {
                    endTime = HOURS;
                }
            }


            System.out.printf("%20s %20d/%d %20d %20dms\n", (startTime + ":00 " + startKey + " - " + endTime + ":00 " +
                            endKey), logList.get(s).getTotalGCount(), logList.get(s).getTotalPCount(),
                    logList.get(s).getUriCount(), logList.get(s).getTotalResponseTime());


        }
    }
}
