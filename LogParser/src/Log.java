/**
 * Created by arafat on 11/2/16.
 */
public class Log implements Comparable {

    int uriCount;
    int totalGCount;
    int totalPCount;
    int totalResponseTime;


    public void setUriCount(int uriCount) {
        this.uriCount = uriCount;

    }

    public int getUriCount() {
        return uriCount;
    }

    public int getTotalGCount() {
        return totalGCount;
    }

    public void setTotalGCount(int totalGCount) {
        this.totalGCount = totalGCount;
    }

    public int getTotalPCount() {
        return totalPCount;
    }

    public void setTotalPCount(int totalPCount) {
        this.totalPCount = totalPCount;

    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }

    @Override
    public int compareTo(Object o) {

        int totalCount = this.getTotalGCount() + this.getTotalPCount();
        Log l = (Log) o;
        int g = l.getTotalGCount() + l.getTotalPCount();
        return g - totalCount;
    }
}