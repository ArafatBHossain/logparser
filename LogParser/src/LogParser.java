import java.util.*;

/**
 * Created by arafat on 11/2/16.
 */
public class LogParser {

    public static void main(String args[]) {

        ParserUtilities pU = new ParserUtilities();
        HashMap<String, Log> listOfAllEntries;
        String logFile = "";
        boolean sort = false;

        if(args.length == 0) {
            System.out.println("Please Enter Name of the Log File: ");
            Scanner scanner = new Scanner(System.in);
            logFile = scanner.nextLine();
            listOfAllEntries = pU.retrieveListOfAllEntries(logFile,sort);
            pU.showSummary(listOfAllEntries);

        }

        else if(args.length==1){
            logFile = args[0];
            listOfAllEntries = pU.retrieveListOfAllEntries(logFile,sort);
            pU.showSummary(listOfAllEntries);

         }


         else if(args.length==2 && args[1].equals("--sort")){
            sort = true;
            logFile = args[0];
            listOfAllEntries = pU.retrieveListOfAllEntries(logFile,sort);
            pU.showSummary(listOfAllEntries);

        }
   }

}
