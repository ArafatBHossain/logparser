import java.util.HashMap;

/**
 * Created by arafat on 11/2/16.
 */
public class LogView {

    public void printLogSummary(HashMap<String, Log> logList){

        System.out.printf("%15s %27s %28s %25s\n","TIME","G/P","Unique URI Count","Total Response Time");

        int startTime,endTime;
        String startKey = "am",endKey="pm";

        for(String s: logList.keySet()){

            startTime = Integer.valueOf(s);
            endTime = Integer.valueOf(s)+1;



            if(startTime>=12){
                startKey = "pm";
                if(startTime>=13){
                    startTime -= 12;
                }

            }else{
                if(startTime==0){
                    startTime=12;
                }
            }


            if(endTime>=12){
                endKey = "pm";
                if(endTime>=13){
                    endTime -= 12;
                }

            }else{
                if(endTime==0){
                    endTime=12;
                }
            }


            System.out.printf("%20s %20d/%d %20d %20dms\n",(startTime+":00 "+startKey+ " - " +endTime+":00 "+endKey),
                    logList.get(s).getTotalGCount(),logList.get(s).getTotalPCount(),logList.get(s).getUriCount(),logList.get(s).getTotalResponseTime());


        }
    }
}
